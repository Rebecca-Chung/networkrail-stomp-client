"""
Created on Wed Apr 13 14:00:46 2016

@author: Erin.Morrow
"""

import stomp, zlib, time, xml
from io import StringIO
from datetime import datetime

#Message processing class
class MyListener(stomp.ConnectionListener):

        msg_list = []
            
        def on_error(self, headers, message):
                self.msg_list.append('(ERROR) ' + message)
                print('received an error %s' % message)

        def on_message(self, headers, message):
				#messages come as zipped xml so decompress them...
                text = zlib.decompress(message, 16+zlib.MAX_WBITS)
                self.msg_list.append(text)                

#Conection configuration for Network Rail
conn = stomp.Connection([('datafeeds.nationalrail.co.uk', 61613)], auto_decode=False)
myListener = MyListener()
conn.set_listener('', myListener)
print("Starting connection...")
conn.start()
#Submit credentials
print("Connecting...")
conn.connect("d3user", "d3password", wait=False)
#Access data feed subscription
print("Subscribing to queue...")
conn.subscribe(destination="/queue/YOUR_QUEUE_ID", id=1, ack='auto')

#Listen to queue for specified conditions
print("Listening...")
t = 0
while len(myListener.msg_list) < 1:
    print(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), " - ", len(myListener.msg_list), " messages")
#    if conn.is_connected():
    try:
        time.sleep(10)
        t = t+10
    except KeyboardInterrupt:
        print("User requested program termination")
        if conn.is_connected():
            print("Connection is live")
            conn.disconnect() 
            conn.stop()
            if conn.is_connected():
                print("Connection is still live")
            else:
                print("Connection terminated by user...")
        break
    
#Deal with collected messages
print(len(myListener.msg_list), " messages collected in ", t, " seconds")
messages = myListener.msg_list
print(myListener.msg_list)

#Clean up connection if necessary
if conn.is_connected():
    print("Connection persisted until end of program")
    conn.disconnect() 
    conn.stop()
    if conn.is_connected():
        print("Connection is still live")
    else:
        print("Connection terminated successfully at close")
else:
    print("Connection was interrrupted prior to close")

#Be polite    
print("Goodbye")


