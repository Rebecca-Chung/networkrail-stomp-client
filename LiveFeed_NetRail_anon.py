"""
Created on Wed Apr 13 14:00:46 2016

@author: Erin.Morrow
@author: Rebecca.Chung (additional code)

"""

import stomp, gzip, xml, time, json
import io
from datetime import datetime
import configparser

# Message processing class
class MyListener(stomp.ConnectionListener):
    msg_list = []

    #        def __init__(self):
    #            self.message = []

    def on_error(self, headers, messages):
        # self.msg_list.append('(ERROR) ' + message)
        print('received an error %s' % messages)

    def on_message(self, headers, messages):
        gzip.GzipFile('./tmp/Stomp_Messages-' + datetime.strftime(datetime.now(),'%H_%M_%S') + '.json.gz','w').write(messages)
        for message in json.loads(messages):
            # if 'CA_MSG' in message:
            self.msg_list.append(message)

#Read Credentials
config = configparser.ConfigParser()
config.read('credential.ini')

# Connection configuration for Network Rail
conn = stomp.Connection([('datafeeds.networkrail.co.uk', 61618)],auto_decode=False, heartbeats = (100000,5000))
myListener = MyListener()
conn.set_listener('', myListener)
print("Starting connection...")
conn.start()
# Submit credentials
print("Connecting...")
conn.connect(config.get('networkrail','username'), config.get('networkrail','password'), wait=False)
# Attach to data feed
print("Subscribing to queue...")
conn.subscribe(destination='/topic/TRAIN_MVT_ALL_TOC', id=1, ack='auto')

# Listening to queue for specified conditions
print("Listening...")
t = 0
while len(myListener.msg_list) < 1000:
    print(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), " - ", len(myListener.msg_list), " messages")
    try:
        time.sleep(30)
        t = t + 10
    except KeyboardInterrupt:
        print("User requested program termination")
        if conn.is_connected():
            print("Connection is live")
            conn.disconnect()
            conn.stop()
            if conn.is_connected():
                print("Connection is still live")
            else:
                print("Connection terminated by user...")
        else:
            print("Connection already closed at time of user request")
        break

# Deal with collected messages
print(len(myListener.msg_list), " messages collected in ", t, " seconds")

# Clean up connection if necessary
if conn.is_connected():
    print("Connection persisted until end of program")
    conn.disconnect()
    conn.stop()
    if conn.is_connected():
        print("Connection is still live")
    else:
        print("Connection terminated automatically at close")
else:
    print("Connection terminated automatically prior to close")

# Be polite
print("Goodbye")